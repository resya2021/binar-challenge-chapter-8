import React, { useState } from "react";

export default function NewPlayerForm({onSubmit}) {
    const [newUsername, setNewUserName] = useState('')
    const [newEmail, setNewEmail] = useState('')
    const [newExperience, setNewExperience] = useState('')
    const [newLevel, setNewLevel] = useState('')

    function handleSubmit(e) {
        e.preventDefault()
        if (newUsername === '' || newEmail === '' || newExperience === '' || newLevel === '') return 
        
        onSubmit(newUsername, newEmail, newExperience, newLevel)

        setNewUserName('')
        setNewEmail('')
        setNewExperience('')
        setNewLevel('')
    }

    return(
        <form onSubmit={handleSubmit} className="new-item-form">
            <div className="form-row">
                <label htmlFor="item">New Player</label>
                <input 
                    value={newUsername} 
                    onChange={e => setNewUserName(e.target.value)} 
                    type="text" 
                    id="item"
                    placeholder="Username"  
                />
                <br />
                <input 
                    value={newEmail} 
                    onChange={e => setNewEmail(e.target.value)} 
                    type="email" 
                    id="item"
                    placeholder="Email"  
                />
                <br />
                <input 
                    value={newExperience} 
                    onChange={e => setNewExperience(e.target.value)} 
                    type="number" 
                    id="item" 
                    placeholder="Experience." 
                />
                <br />
                <input 
                    value={newLevel} 
                    onChange={e => setNewLevel(e.target.value)} 
                    type="number" 
                    id="item" 
                    placeholder="Level" 
                />
            </div>
            <br />
            <button type="submit" className="btn">Add</button>
        </form>
    )
}